import java.util.ArrayList;
import java.util.List;

public class main {
    public static void main(String[] args) {
        List<Figure> figureList = new ArrayList<>();
        figureList.add(new Rectangle("Прямоугольник",4,2, 0, 0));
        figureList.add(new Circle("Круг",0,0, 3, 0, 0));
        figureList.add(new Triangle("Треугольник",4, 5, 4, 0, 0));
        for (Figure a:figureList) {
            System.out.printf(a.toString());
        }
    }
}
